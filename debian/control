Source: libtest-needsdisplay-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Damyan Ivanov <dmn@debian.org>,
           gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13), libmodule-install-perl
Build-Depends-Indep: libtest-pod-perl,
                     libtest-pod-coverage-perl,
                     libtest-cpan-meta-perl,
                     libtest-minimumversion-perl,
                     perl,
                     xauth,
                     xvfb
Standards-Version: 3.9.6
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libtest-needsdisplay-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libtest-needsdisplay-perl.git
Homepage: https://metacpan.org/release/Test-NeedsDisplay

Package: libtest-needsdisplay-perl
Architecture: all
Depends: ${perl:Depends},
         ${misc:Depends}
Recommends: xauth,
            xvfb
Description: module to ensure that tests needing a display have one
 When testing GUI applications, sometimes applications or modules absolutely
 insist on a display, even just to load a module without actually showing any
 objects.
 .
 Regardless, this makes GUI applications pretty much impossible to build and
 test on headless or automated systems. And it fails to the point of not even
 running the Makefile.PL script because a dependency needs a display so it can
 be loaded to find a version.
 .
 In these situations, what is needed is a fake display.
 .
 The Test::NeedsDisplay module will search around and try to find a way to load
 some sort of display that can be used for the testing.
